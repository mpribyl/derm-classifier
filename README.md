# DermKlasifikátor

Implementace klasifikačního modelu dermoskopických obrázků, který dokáže rozpoznat z daných obrázků
nádorová onemocnění. Daný klasifikační problém vychází ze [soutěže ISIC 2019](https://challenge2019.isic-archive.com/). 

## Model ke stažení
Váhy trénovaného modelu jsou ke stažení na adrese [https://drive.google.com/file/d/1OSqDF_CtZo3uYjjSJUi26x03i0VE8CN_/view?usp=sharing](https://drive.google.com/file/d/1OSqDF_CtZo3uYjjSJUi26x03i0VE8CN_/view?usp=sharing). Tento model byl trénován po dobu 30 epoch bez augmentovaných dat.

## Dataset
Dataset je třeba stáhnout ze stránek soutěže na 
[https://challenge2019.isic-archive.com/data.html](https://challenge2019.isic-archive.com/data.html). Dataset je rozdělen
do trénovací a testovací části. V trénovací části jsou obsaženy obrázky, labely a metadata. Testovací část pak 
obsahuje pouze obrázky a metadata. Trénovací data obsahují 25331 obrázků s 8 třídami viz tabulka níže.

<table>
  <tr>
    <th>třída</th>
    <td>MEL</td>
    <td>NV</td>
    <td>BCC</td>
    <td>AK</td>
    <td>BKL</td>
    <td>DF</td>
    <td>VASC</td>
    <td>SCC</td>
  </tr>
  <tr>
    <th>počet obrázků</th>
    <td>4522</td>
    <td>12875</td>
    <td>3323</td>
    <td>867</td>
    <td>2624</td>
    <td>239</td>
    <td>253</td>
    <td>628</td>
  </tr>
</table>


* předzpracování datasetu do požadované struktury:

```bash
python dataset_utils.py create \
--images_dir ./isic_dataset_2019/train/images/ \
--images_labels_path ./isic_dataset_2019/train/train_GroundTruth.csv \
--train_size 0.9 \
--output ./my_preprocessed_dataset/
```

* předzpracování obrázků:

```bash
python preprocess_images.py \
--images_dir ./my_preprocessed_dataset/train/images/ \
--target_image_resolution 224 \
--with_color_constancy=true \
--output ./my_preprocessed_dataset/train/images/

python preprocess_images.py \
--images_dir ./my_preprocessed_dataset/val/images/ \
--target_image_resolution 224 \
--with_color_constancy=true \
--output ./my_preprocessed_dataset/val/images/
```

## Trénování

* spuštění trénování modelu:

```bash
python train.py \
--train_images_dir ./my_preprocessed_dataset/train/images/ \
--val_images_dir ./my_preprocessed_dataset/val/images/ \
--train_batch_size 128 \
--val_batch_size 128 \
--epochs 30 \
--image_resolution 224 \
--pretrained_model "en_b0" \
--exp_dir ./logs \
--exp_name experiment1 \
--lr_drop_every 5
```

* vytvoření shrnutí z proběhlého trénování:

```bash
python train_summary.py \
--train_metrics_loss_path ./logs/experiment1/run_0/train_metrics_and_loss.csv \
--val_metrics_loss_path ./logs/experiment1/run_0/val_metrics_and_loss.csv \
--output_dir ./logs/experiment1/run_0/
```

## Augmentace dat
Pomocí skriptu `augment.py` je možné vytvořit augmentovaná data z původního datasetu.

* vytvoření augmentovaných dat:
```bash
python augment.py \
--input_images_dir ./my_preprocessed_dataset/train/images/ \
--output_dir ./my_preprocessed_dataset/train/augmented_images/ \
--num_images 1000 \
--batch_size 64 \
--target_img_width 224 \
--target_img_height 224
```

